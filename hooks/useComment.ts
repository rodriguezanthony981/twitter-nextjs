import { useCallback, useMemo } from "react";

import useCurrentUser from "./useCurrentUser"
import useLoginModal from "./useLoginModal";
import usePost from "./usePost";
import usePosts from "./usePosts";

const useComment = ({ postId, userId } : { postId: string, userId?: string }) => {
  const { data: currentUser } = useCurrentUser();
  const { data: fetchedPost, mutate: mutateFetchedPost } = usePost(postId);

  
  const commented = useCallback((post: any) =>{
    //post.stopPropagation();
    try {
      if (post?.user?.id === currentUser?.id) {
        return true;
      } 
    } catch (error) {
      return false
    }
    
  }, [currentUser?.id]);

  const hasComment = useMemo(() => {
    
    const list = fetchedPost?.comments|| [];
      //644c8215106e6e7b8502c292 id
    let ready = list.find((post: any) => commented(post));
    
    if (typeof ready === "undefined"){
      return false
    }
    return true;
    
  }, [fetchedPost, commented]);

  return { 
    hasComment,
  }
};

export default useComment;
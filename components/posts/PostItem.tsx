import React, { useCallback, useMemo } from 'react'
import { useRouter } from 'next/router';
import { formatDistanceToNowStrict } from 'date-fns';
import { AiOutlineHeart, AiFillHeart, AiOutlineMessage, AiFillMessage } from 'react-icons/ai';
import { GiBackwardTime } from 'react-icons/gi'

import useCurrentUser from '@/hooks/useCurrentUser';
import useLoginModal from '@/hooks/useLoginModal';
import useLike from '@/hooks/useLike';

import Avatar from '../Avatar';
import useComment from '@/hooks/useComment';

interface PostItemProps {
  data: Record<string, any>;
  userId?: string;
}

const PostItem: React.FC<PostItemProps> = ({ data = {}, userId }) => {
  const router = useRouter();
  const loginModal = useLoginModal();
  
  const { data: currentUser } = useCurrentUser();
  
  const { hasLiked, toggleLike } = useLike({ postId: data.id, userId });
  const { hasComment } = useComment({ postId: data.id, userId });
  
  const goToUser = useCallback((event: any) => {
    event.stopPropagation();

    router.push(`/users/${data.user.id}`);
  }, [router, data.user.id]);

  const goToPost = useCallback(() => {
    router.push(`/posts/${data.id}`)
  }, [router, data.id])

  const onLike = useCallback((event: any) => {
    event.stopPropagation();

    if (!currentUser) {
      return loginModal.onOpen();
    }

    toggleLike();
  }, [currentUser, loginModal, toggleLike]);

  const createdAt = useMemo(() => {
    if (!data?.createdAt) {
      return null;
    }

    return formatDistanceToNowStrict(new Date(data.createdAt));
  }, [data?.createdAt]);

  const LikeIcon = hasLiked ? AiFillHeart : AiOutlineHeart;
  const MessageIcon = hasComment ? AiFillMessage : AiOutlineMessage;

  return (
    <div
      onClick={goToPost}
      className='
        border-b-[1px]
        border-neutral-800
        px-3
        pb-0 pt-3
        cursor-pointer
        hover:bg-neutral-900
        transition
      '
    >
      <div className='flex flex-row items-start gap-3'>
        <Avatar userId={data.user.id} />
        <div >
          <div
            className='
              flex flex-row items-center gap-2
            '
          >
            <p  
              onClick={goToUser}
              className='
                text-white
                font-semibold
                cursor-pointer
                hover:underline
              '
            >{data.user.name}</p>
            <span 
              onClick={goToUser}
              className="
              text-neutral-500
              cursor-pointer
              hover:underline
              hidden
              md:block
            ">
              @{data.user.username}
            </span>
            
            <span className="flex flex-row text-neutral-500 text-sm hover:text-white transition">
              <GiBackwardTime size={20}/> 
              {createdAt}
            </span>
          </div>
          <div className='text-white mt-1'>
            {data.body}
          </div>
          <div className='flex flex-row items-center mt-1 gap-6'>
            <div
              className='
                flex flex-row
                items-center
                text-neutral-500
                gap-2
                cursor-pointer
                transition
                hover:text-sky-500
              '
            >
              
              <div 
                className={`
                  w-10 h-10 
                  hover:bg-sky-500 hover:bg-opacity-20
                  rounded-full 
                  grid justify-center items-center
                  p-0 m-0`}>
                <MessageIcon className={hasComment ? 'text-sky-500' : ''} size= {20} />
              </div>
              <p>
                {data.comments?.length || 0}
              </p>
            </div>
            <div
              onClick={onLike}
              className='
                flex flex-row
                items-center
                text-neutral-500
                gap-1
                cursor-pointer
                transition
                hover:text-red-700
              '
            >
              <div 
                className={`
                  w-10 h-10 
                  hover:bg-red-700 hover:bg-opacity-20
                  focus:bg-red-700 focus:bg-opacity-20
                  peer-focus:bg-red-700 peer-focus:bg-opacity-20
                  rounded-full 
                  grid justify-center items-center
                  p-0 m-0`}>
                <LikeIcon size= {20} color={hasLiked ? 'red' : ''} />
              </div>
              <p className='peer'>
                {data.likedIds.length || 0}
              </p>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  )
}

export default PostItem